# compile
ant debug

# create build artifact
cp bin/classes.jar bin/tidahl.jar

# create javadoc files
mkdir gen.doc
mkdir bin
javadoc -noqualifier java.lang -d gen.doc/ -public -sourcepath src/ -subpackages com.tmobile.tmoid.helperlib -exclude com.tmobile.tmoid.helperlib.impl:com.tmobile.tmoid.helperlib.util

# create javadoc jar file
cd gen.doc
zip -r ../bin/tidahl.doc.jar *
cd ..

# create tidahl.src.jar
cd src
zip -r ../bin/tidahl.src.jar *
cd ..

package com.tmobile.tmoid.helperlib;

/**
 * AccessTokenExpiredException is thrown when the backend service finds out
 * that the access token is expired. The application that was making a call
 * needs to get a new token before retrying.
 *
 * <p>Application developer is usually not expected to create instances of this class directly.</p>
 */
public class AccessTokenExpiredException extends IAMException {
    public AccessTokenExpiredException() {
    }

    public AccessTokenExpiredException(Throwable throwable) {
        super(throwable);
    }
}

package com.tmobile.tmoid.helperlib;

/**
 * This class contains the fields for access token. It's basically just a collection
 * of fields without any real business logic. Application developer may create instances
 * of this class if one serializes the data somewhere and loads it later. Usually
 * instances of this class are received as return value for {@link com.tmobile.tmoid.helperlib.Agent#requestAccessToken}.
 */
public class AccessToken {
    private String token;
    private int expiresIn;
    private String tmoId;
    private String scope;
    private long timeStamp;
    private String refreshToken;
    private String tokenType;

    /**
     * Create a new access token. Typically the values are loaded from permanent storage
     * and extracted from another instance of this class earlier.
     *
     * @param token Access token value as string.
     * @param expiresIn Lifetime of the token in seconds
     * @param tmoId Hashed username of the account
     * @param scope Granted scope of this token
     * @param timestamp Local timestamp when the HTTP request was made to grant this token
     * @param refreshToken RefreshToken associated to this token
     * @param tokenType of token
     */
    public AccessToken(String token, int expiresIn, String tmoId, String scope, long timestamp, String refreshToken, String tokenType) {
        this.token = token;
        this.expiresIn = expiresIn;
        this.tmoId = tmoId;
        this.scope = scope;
        this.timeStamp = timestamp;
        this.refreshToken = refreshToken;
        this.tokenType = tokenType;
    }

    /**
     * Get the string value of this token
     *
     * @return Access token as string
     */
    public String getToken() {
        return token;
    }

    // Setters are not useful for this class. I'll comment them out, since
    // their purpose is not evident from the API point of view.

    /*public void setToken(String token) {
        this.token = token;
    }*/

    /**
     * Get the token lifetime in seconds
     *
     * @return Token lifetime
     */
    public int getExpiresIn() {
        return expiresIn;
    }

    /*public void setExpiresIn(int expiresIn) {
        this.expiresIn = expiresIn;
    } */

    /**
     * Get the hashed user identifier for the T-Mobile ID account this token
     * is related to.
     *
     * @return Hashed user identifier
     */
    public String getTmoId() {
        return tmoId;
    }
/*
    public void setTmoId(String tmoId) {
        this.tmoId = tmoId;
    }
*/
    /**
     * Get scope for this token. The scope can contain several values separated
     * by spaces, so it's good idea to use string search instead of comparison
     * for equality:
     *
     * <pre>
     * AccessToken token = ...;
     * if (token.getScope().contains(Agent.SCOPE_ENTITLEMENTS)) {
     *     // has entitlements
     * }
     * </pre>
     *
     * @return Scope
     */
    public String getScope() {
        return scope;
    }
/*
    public void setScope(String scope) {
        this.scope = scope;
    }
*/
    /**
     * Get the local timestamp when this token was requested.
     *
     * @return Timestamp as reported by {@link System#currentTimeMillis}.
     */
    public long getTimeStamp() {
        return timeStamp;
    }
/*
    public void setTimeStamp(long timestamp) {
        this.timeStamp = timestamp;
    }
*/
    public String getRefreshToken() {
        return refreshToken;
    }
/*
    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }
*/
    public String getTokenType() {
        return tokenType;
    }
/*
    public void setTokenType(String tokenType) {
        this.tokenType = tokenType;
    }
*/
    /**
     * Locally calculates an estimate whether the token is still valid or not, 
     * using the current time and the values found within the token. This will 
     * not do any server access in order to quarantee validity.
     *
     * @return Whether the token has expired or not.
     */
    public boolean isExpired() {
        return System.currentTimeMillis() > (timeStamp + 1000 * expiresIn);
    }

    @Override
    public String toString() {
        return "AccessToken{" +
                "token='" + token + '\'' +
                ", expiresIn=" + expiresIn +
                ", tmoId='" + tmoId + '\'' +
                ", scope='" + scope + '\'' +
                ", timeStamp=" + timeStamp +
                ", refreshToken='" + refreshToken + '\'' +
                ", tokenType='" + tokenType + '\'' +
                '}';
    }

//    public String getToken();
//    public int getExpiresIn();
//    public String getTmoId();
//    public String getScope();
//    public long getTimeStamp();
//    public String getRefreshToken();
//    public String getTokenType();
//
//
//    public void setToken(String token);
//    public void getExpiresIn(int expiresIn);
//    public void getTmoId(String tmoid);
//    public void getScope(String scope);
//    public void getTimeStamp(long timestamp);
//    public void getRefreshToken(String refreshToken);
//    public void getTokenType(String tokenType);
}

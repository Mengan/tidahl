package com.tmobile.tmoid.helperlib;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * ConsumerProfile class holds the user's profile data as JSON-markup
 * from the T-Mobile profile server. This class allows the helper library
 * to construct JSON-data from Binder IPC Bundle; from the application
 * developer's view ConsumerProfile behaves just like normal JSONObject.
 *
 * The profile data could look like following. The exact contents depend on
 * how the application is provisioned at the server side and which scope
 * is asked in the access token.
 *
 * <pre>
 * "tmobileid" : "45730458390eflkjsdf",
 * "TMO_ID_profile":
 * {
 *     "firstName": "Jane",
 *     "lastName": "Doe",
 *     "email": janedoe@example.com,
 *     "userName" :"bobyjoe",
 *     "impu1" : "sip:bobyjoe@tmobileid.com",
 *     "openId" : "bobyjoe@tmobileid.com",
 *     "origTokenId" :"1234567890901223345561123234454",
 *     "accountCreationDate" : "1354595842",
 *     "isTMO" : "TRUE",
 *     ...
 * }
 * "associated_lines":
 * {
 *     "defaultTMOAccount" : "line1",
 *     "lines" :
 *     [
 *         {
 *             "lineId" : "line1",
 *             "isGBA" : "TRUE",
 *             "GBABTID" : "34uwfrjfewoureorwe",
 *             "MSISDN" : "tel:+15555555555",
 *             "customerType" : "postpaid",
 *             "accountStatus" : "active",
 *             "customerId" : "423523525",
 *             "BAN" : "3432050"
 *         },
 *         {
 *             "lineId" : "line2",
 *             "isGBA" : "FALSE",
 *             "GBABTID" : "",
 *             "MSISDN" : "tel:+15555555556",
 *             "customerType" : "prepaid",
 *             "accountStatus" : "active",
 *             "customerId" : "23794230",
 *             "BAN" : "3432050",
 *         }
 *     ]
 * }
 * "billing_information":
 * {
 *     "billingPreference" : "method1",
 *     "methods" :
 *     [
 *         {
 *             "methodId" : "method1",
 *             "paymentType" : "TMO_Direct",
 *             "paymentAccount" : "tel:+15555555555"
 *         },
 *         {
 *             "methodId" : "method2",
 *             "paymentType" : "PayPal",
 *             "paymentAccount" : "xxx"
 *         }
 *     ]
 * }
 * "entitlements":
 * [
 *     {
 *         "entitlementName" : "{entitlement name}",
 *         "entitlementEval": "TRUE",
 *         "entitlementDesc" : "Subscriber entitled for Offer XYZ"
 *     },
 *     {
 *         "entitlementName" : "{entitlement name}",
 *         "entitlementEval": "FALSE",
 *         "entitlementDesc" : "Subscriber not entitled for Offer ABC"
 *     }
 * ]
 * </pre>
 *
 * <p>So, the profile may contain five sections:</p>
 * <ul>
 * <li>tmobileid: anonymous, hashed id. Can be tested for equality, but does not reveal the user</li>
 * <li>TMO_ID_profile: User's profile</li>
 * <li>associated_lines: Different phone numbers of the user</li>
 * <li>billing_information</li>
 * <li>entitlements</li>
 * </ul>
 * 
 * The method reference for the base class can be found from 
 * <a href="http://json.org/javadoc/org/json/JSONObject.html">
 * http://json.org/javadoc/org/json/JSONObject.html</a>.
 */
public class ConsumerProfile extends JSONObject {
    /**
     * The constructor is not expected to be called directly by 3rd-party
     * programmers. One will receive an instance of this class as a
     * return value from {@link Agent#requestConsumerProfile}.
     */ 
    public ConsumerProfile(String json) throws JSONException {
        super(json);
    }
}

package com.tmobile.tmoid.helperlib;

/**
 * This exception may be thrown when an application was requesting an access
 * token and the request required an UI to popup. However, the UI operation
 * was not completed. The application may retry the original request later.
 * <p>Application developer is usually not expected to create instances of this class directly.</p>
 */
public class RequestCanceledException extends IAMException {
    public RequestCanceledException() {
    }

    public RequestCanceledException(Throwable throwable) {
        super(throwable);
    }
}

package com.tmobile.tmoid.helperlib;

/**
 * Thrown to indicate that an operation could not complete because the communication with Device Agent failed:
 * <ul><li>the DeviceAgent service has died because its hosting process no longer exists
 * (for example, it was killed or application was uninstalled).</li></ul>
 *
 * <p>Application developer is usually not expected to create instances of this class directly.</p>
 */
public class CommunicationException extends IAMException {
    public CommunicationException() {
    }

    public CommunicationException(Throwable throwable) {
        super(throwable);
    }
}

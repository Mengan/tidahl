package com.tmobile.tmoid.helperlib;

/**
 * <p>Agent class allows the application to discuss with the Device Agent
 * service. The application can request access tokens and
 * perform profile lookups. Also the received tokens can be used to authenticate
 * for 3rd party services if they allow T-Mobile ID as authentication method.</p>
 *
 * <p>In order to get an instance of this class, you need to connect to the 
 * Device Agent service using {@link AgentServiceConnection}.</p>
 *
 * <p>Access token related functions {@link Agent#requestAccessToken}
 * and {@link Agent#requestConsumerProfile} possibly include network traffic, so they
 * are not expected to be called from the main thread.</p>
 */
public abstract class Agent {
    public static final String SCOPE_TMO_ID_PROFILE = "TMO_ID_profile";
    public static final String SCOPE_ASSOCIATED_LINES = "associated_lines";
    public static final String SCOPE_BILLING_INFORMATION = "billing_information";
    public static final String SCOPE_ENTITLEMENTS = "entitlements";
    /**
     * This is the default type of token.
     */
    public static final String ACCESS_TYPE_ONLINE = "ONLINE";
    /**
     * Offline type leads into longer lasting tokens.
     */
    public static final String ACCESS_TYPE_OFFLINE = "OFFLINE";
    /**
     * No UI is expected when performing authorization operation. This type
     * of operation will fail if user interaction would be required.
     */
    public static final String DISPLAY_NONE = "NONE";
    /**
     * Authorization will be show the UI.
     */
    public static final String DISPLAY_PAGE = "PAGE";
    /**
     * Authorization will be attempted without the UI first, but the operation
     * will fallback to UI mode if needed.
     */
    public static final String DISPLAY_NONE_OR_PAGE = "NONE|PAGE";
    public static final String REAUTH_AUTO = "auto";
    public static final String REAUTH_FORCE = "force";

    public static final String APPROVAL_PROMPT_AUTO = "auto";
    public static final String APPROVAL_PROMPT_FORCE = "force";

    protected Agent() { };

    /** 
     * Queries the Device Agent whether an user is currently authenticated or not.
     * returns true if authentication is performed, false otherwise
     *
     * @return Whether an user is currently authenticated
     *
     * @throws CommunicationException
     */
    public abstract boolean requestAuthenticationStatus() throws CommunicationException;

    /**
     * Queries the Device Agent about the current user name, if known. 
     * The name is the plain-text value and intended to hint the user which account
     * is currently used.
     *
     * @return Which user account name is currently used.
     *
     * @throws CommunicationException
     */
    public abstract String requestUserIdentifier() throws CommunicationException;

    /**
     * <p>Requests the Device Agent to authorize (and authenticate if needed)
     * the user. Authorization will trigger network traffic, so calling this function
     * is not recommended from the main thread of the application.</p>
     * <p>This variant allows the application to define explicit values for
     * client id, client secret, scope, access_type, display and reauth.
     * <p>This method causes the calling thread to wait uninterruptible until device agent sends the authorization code.</p>
     *
     * @param client_id Client id
     * @param scope Any of {@link #SCOPE_ASSOCIATED_LINES}, {@link #SCOPE_BILLING_INFORMATION}, {@link #SCOPE_ENTITLEMENTS} or {@link #SCOPE_TMO_ID_PROFILE}.
     *              Multiple scopes can be requested by concatenating individual values separated by a whitespace " "
     * @param access_type Either {@link #ACCESS_TYPE_ONLINE} or {@link #ACCESS_TYPE_OFFLINE}. This affects the lifetime of the granted token.
     * @param display Allows the caller to define what kind of UI activity is expected, can be {@link #DISPLAY_NONE}, {@link #DISPLAY_PAGE}
     *                or {@link #DISPLAY_NONE_OR_PAGE}. Requiring {@link #DISPLAY_NONE} while user interaction would be required leads into exception thrown.
     * @param reauth Either {@link #REAUTH_AUTO} or {@link #REAUTH_FORCE}.
     * @param approval_prompt Either {@link #APPROVAL_PROMPT_AUTO} or {@link #APPROVAL_PROMPT_FORCE}
     * @return Authorization code Matching the granted scope
     */
    public abstract String requestAuthorizationCode(String client_id, String scope,
            String access_type, String display, String reauth, String approval_prompt)
            throws RequestCanceledException, CommunicationException, ServerErrorException, InvalidStateException;

    /**
     * <p>Requests the Device Agent to fetch access token based on the specified authorization code
     * Requesting an access token will trigger network traffic, so calling this function
     * is not recommended from the main thread of the application.</p>
     * <p>This variant allows the application to define explicit value for client_id
     * <p>This method causes the calling thread to wait uninterruptible until device agent sends the access token.</p>
     *
     * @param client_id Client id
     * @param client_secret Client secret
     * @param authorization_code The authorization code
     * @return AccessToken Matching the granted scope
     */
    public abstract AccessToken requestAccessToken(
            String client_id, String client_secret, String authorization_code)
            throws CommunicationException, RequestCanceledException, ServerErrorException;

    /**
     * <p>Refreshes the given access token to include the new scope using refresh token
     * from previous request.
     * This will trigger network traffic, so calling this function
     * is not recommended from the main thread of the application.</p>
     *
     * @param refresh_token
     * @param scope
     *
     * @return AccessToken
     */
    protected abstract AccessToken refreshAccessToken(String refresh_token, String scope);

    /**
     * <p>Refreshes the given access token with the existing scope using refresh token
     * from previous request.
     * This will trigger network traffic, so calling this function
     * is not recommended from the main thread of the application.</p>
     *
     * @param refresh_token
     * @return APIResponse
     */
    protected abstract AccessToken refreshAccessToken(String refresh_token);

    /**
     * <p>Performs a profile lookup based on the scope defined by the access token.
     * The response value contains object holding the JSON-markup.
     * The operation will trigger network traffic, so calling this function
     * is not recommended from the main thread of the application.</p>
     *
     * @param access_token The previously received access token string.
     * @see AccessToken#getToken
     * @return Subset of the profile containing the sections defined by the given access token. 
     */
    public abstract ConsumerProfile requestConsumerProfile(AccessToken access_token)
            throws CommunicationException, ServerErrorException, AccessTokenExpiredException;
}

package com.tmobile.tmoid.helperlib;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.*;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.net.Uri;
import android.os.IBinder;
import android.os.Looper;
import com.tmobile.tmoid.helperlib.impl.AgentImpl;
import com.tmobile.tmoid.helperlib.impl.Constants;
import com.tmobile.tmoid.helperlib.impl.IHelperLibrary;
import com.tmobile.tmoid.helperlib.util.Log;

import java.util.Locale;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

/**
 * <p>AgentServiceConnection is the starting point for application developers.
 * This class takes care of binding to remote AIDL-service and provides utility to
 * direct user to Google Play in a case the dependant service is not found.</p>
 * <p/>
 * Binding to service is asynchronous by nature on Android: Only system services,
 * which are always running, are accessible in synchronous fashion (i.e you can
 * use them immediately on the next line of the code). Binding
 * process involves ServiceConnection class, whose callbacks are always executed
 * in the main thread of the application. AgentServiceConnection provides alternative
 * ways to bind to the service:</p>
 * <ul>
 * <li>Blocking operation: This one waits until the binding completes. The value
 * for Agent is available at the following line. Trying to call this from
 * the main thread would lead into deadlock. So, RuntimeException
 * is thrown if one uses the blocking variant from the main thread.</li>
 * <li>Asynchronous operation: This one just triggers the binding operation, which
 * completes at some point in the future (after the execution enters the main idle loop).
 * The application will provide a listener to be called when the binding finishes.</li>
 * </ul>
 * <p/>
 * <pre>
 * public class MyActivity extends Activity
 *         implements AgentServiceConnection.ConnectedCallback {
 * ...
 *     public void onCreate(Bundle savedInstanceState) {
 *     ...
 *         try {
 *             mConnection = new AgentServiceConnection(getApplicationContext(), this);
 *         } catch(AgentNotFoundException e)
 *             AgentServiceConnection.handleAgentNotFound(this);
 *         }
 *     }
 * ...
 *     public void onServiceConnected(AgentServiceConnection serviceConnection) {
 *         try {
 *             mAgent = serviceConnection.getAgent();
 *         } catch (AgentNotFoundException e) {
 *             AgentServiceConnection.handleAgentNotFound(this);
 *         }
 *     }
 * ...
 * }
 * </pre>
 */
public class AgentServiceConnection {
    private static final String LOG_TAG = Constants.LOG_TAG;
    private static final String ENGLISH_ALERT = "needs to use the T-Mobile ID authentication library. Please install this free library from the Google Play marketplace";
    private static final String SPANISH_ALERT = "necesita utilizar la biblioteca de autentificación de identidad de T-Mobile. Favor de instalar esta biblioteca gratuita del sitio de Google Play.";

    private static final String LIBRARY_VERSION = "1.4";

    private AgentServiceState mServiceState = AgentServiceState.NOT_INITIALIZED;

    ServiceConnection service_connection;
    Semaphore semaphore = new Semaphore(0);
    volatile Agent helperLibrary;

    /**
     * <p>Empty constructor.</p>
     *
     * @deprecated Intended for use in testing scenarios (when a mock connection is needed) only
     */

    public AgentServiceConnection() { }

    /**
     * Blocking constructor. This one binds to Device Agent service and waits
     * until the operation completes.
     * Note: This must be called from non main thread
     *
     * @param ctx Android Context that is used for binding to service.
     */
    public AgentServiceConnection(Context ctx) throws AgentNotFoundException, AgentConnectionException {
        this(ctx, 0, null);
    }

    /**
     * <p>Async constructor. This variant initiates the binding operation. The given
     * callback will be triggered once the operation is ready.</p>
     * <p><b>Note!</b>The binding completion will be reported after the main thread
     * enters the event loop. So, trying to wait the operation completion in a function
     * run by main thread will never complete (this would be a deadlock).</p>
     *
     * @param ctx Android Context that is used for binding to service.
     * @param callback The callback object to be activated once the binding completes
     */
    public AgentServiceConnection(Context ctx, final ConnectedCallback callback) throws AgentNotFoundException {
        // Async variant can be called from the main thread
        //check_main_thread();

        check_agent_presence(ctx);

        Log.init(ctx, Log.LoggingMode.REMOTE_VIA_IPC, LOG_TAG);

        service_connection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                Log.d(LOG_TAG, "onServiceConnected(thread:" + Thread.currentThread().getId() + ")");
                helperLibrary = new AgentImpl(IHelperLibrary.Stub.asInterface(service));
                mServiceState = AgentServiceState.CONNECTED;
                callback.onServiceConnected(AgentServiceConnection.this);
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {
                Log.d(LOG_TAG, "onServiceDisconnected(thread:" + Thread.currentThread().getId() + ")");
                mServiceState = AgentServiceState.DISCONNECTED;
                helperLibrary = null;
            }
        };

        bindToIamService(ctx);
    }

    private void bindToIamService(Context ctx) {
        Intent intent = new Intent("com.tmobile.tmoid.helperlib.IHelperLibrary");
        // support for API_LEVEL >= 21 (LOLLIPOP)
        intent.setPackage("com.tmobile.tmoid.agent");
        intent.putExtra("version", LIBRARY_VERSION);
        ctx.bindService(intent, service_connection, Context.BIND_AUTO_CREATE);

        Log.i(LOG_TAG, "binding to IAM Service with LIBRARY_VERSION: " + LIBRARY_VERSION);
    }

    /**
     * Blocking constructor with a timeout. Note: This must not be called from the main thread
     * 
     * @param ctx Android Context that is used for binding to service.
     * @param timeout the maximum time to wait for the service connection
     * @param timeUnit the time unit of the {@code timeout} argument, for example {@link java.util.concurrent.TimeUnit#SECONDS}.
     */
    public AgentServiceConnection(Context ctx, long timeout, TimeUnit timeUnit) throws AgentNotFoundException, AgentConnectionException {
        check_main_thread();

        check_agent_presence(ctx);

        Log.init(ctx, Log.LoggingMode.REMOTE_VIA_IPC, LOG_TAG);

        Log.d(LOG_TAG, "Creating AgentServiceConnection");
        connect_to_iam_service(ctx);

        if (timeout == 0) {
            semaphore.acquireUninterruptibly();
        } else {
            Log.d(LOG_TAG, "tryAcquire start(thread:" + Thread.currentThread().getId() + ")");
            try {
                semaphore.tryAcquire(timeout, timeUnit);
            } catch (InterruptedException e) {
            }
            Log.d(LOG_TAG, "tryAcquire done, helperlib (thread:" + Thread.currentThread().getId() + ")" + helperLibrary);
        }

        if (service_connection == null || helperLibrary == null) throw new AgentConnectionException();
    }

    private void check_agent_presence(Context ctx) throws AgentNotFoundException {
        try {
            ctx.getPackageManager().getPackageInfo(Constants.AGENT_PACKAGE_NAME, PackageManager.GET_ACTIVITIES);
        } catch (PackageManager.NameNotFoundException e) {
            throw new AgentNotFoundException(e);
        }
    }

    private void check_main_thread() {
        if (Looper.myLooper() == Looper.getMainLooper()) {
            throw new RuntimeException("Cannot run this constructor on main thread. For that, use async alternative.");
        }
    }

    private void connect_to_iam_service(Context ctx) {
        service_connection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                Log.d(LOG_TAG, "onServiceConnected(thread:" + Thread.currentThread().getId() + ")");
                helperLibrary = new AgentImpl(IHelperLibrary.Stub.asInterface(service));
                mServiceState = AgentServiceState.CONNECTED;
                semaphore.release();
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {
                Log.d(LOG_TAG, "onServiceDisconnected(thread:" + Thread.currentThread().getId() + ")");
                mServiceState = AgentServiceState.DISCONNECTED;
                helperLibrary = null;
            }
        };

        bindToIamService(ctx);
    }

    /**
     * This function returns reference to Helper library API once the binding
     * to remote service has succeeded.
     *
     * @return Reference to {@link Agent}.
     */
    public Agent getAgent() throws AgentConnectionException, AgentStateException {
        if (helperLibrary == null) {
            switch (mServiceState) {
                case NOT_INITIALIZED:
                    throw new AgentStateException();
                default:
                    throw new AgentConnectionException();
            }
        }

        return helperLibrary;
    }

    /**
     * This function presents a dialog that informs the remote service is not
     * installed. User is taken to Google Play for installation. This function
     * accesses UI components, so it needs to be called from the main thread
     * of the application.
     *
     * @param activity The dialog will appear on top of this Activity.
     */
    public static void handleAgentNotFound(final Activity activity) {
        Resources res = activity.getResources();
        String application_name = res.getString(activity.getApplicationInfo().labelRes);
        String agentNotFoundText = ENGLISH_ALERT;

        // Set alert message to Spanish
        if (Locale.getDefault().getLanguage().equalsIgnoreCase("es"))
            agentNotFoundText = SPANISH_ALERT;

        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setMessage(application_name + " " + agentNotFoundText)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Intent i = new Intent(android.content.Intent.ACTION_VIEW);
                        i.setData(Uri.parse("https://play.google.com/store/apps/details?id=com.tmobile.tmoid.agent"));
                        activity.startActivity(i);

                        activity.finish();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    /**
     * ConnectedCallback is used by asynchronous variant of {@link AgentServiceConnection}
     * to inform the caller that the remote service is bound. Caller can
     * use {@link AgentServiceConnection#getAgent} to receive the helper
     * library reference. This callback is invoked in the main thread of the application.
     */
    public static interface ConnectedCallback {
        public void onServiceConnected(AgentServiceConnection serviceConnection);
    }


    private enum AgentServiceState {
        NOT_INITIALIZED, CONNECTED, DISCONNECTED
    }
}

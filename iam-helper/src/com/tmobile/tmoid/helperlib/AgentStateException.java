package com.tmobile.tmoid.helperlib;

/**
 * Thrown when {@link com.tmobile.tmoid.helperlib.AgentServiceConnection#getAgent()} is called before
 * the connection to the IAM Device Agent application has been initialized.
 *
 * <p>Application developer is usually not expected to create instances of this class directly.</p>
 */
public class AgentStateException extends IAMException {

}

package com.tmobile.tmoid.helperlib;

/**
 * Thrown to indicate that an operation could not complete because the Agent state is invalid.
 * E.G. The 3rd party app is requesting an authorization code but the user is not signed in or the user session expired
 * <p>Application developer is usually not expected to create instances of this class directly.</p>
 */
public class InvalidStateException extends IAMException {
    public InvalidStateException() {
    }

    public InvalidStateException(String detailMessage) {
        super(detailMessage);
    }

    public InvalidStateException(Throwable throwable) {
        super(throwable);
    }
}

package com.tmobile.tmoid.helperlib;

/**
 * Thrown by {@link com.tmobile.tmoid.helperlib.AgentServiceConnection#getAgent()} in case
 * the connection to the IAM Device Agent service has been lost.
 *
 * <p>Application developer is usually not expected to create instances of this class directly.</p>
 */
public class AgentConnectionException extends IAMException {

}

package com.tmobile.tmoid.helperlib.impl;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.*;
import java.util.Iterator;

/**
 * ConsumerProfileIPC class holds the user's profile data as JSON-markup
 * from the T-Mobile profile server. This class allows the helper library 
 * to construct JSON-data from Binder IPC Bundle; from the application 
 * developer's view ConsumerProfileIPC behaves just like normal JSONObject.
 *
 * The profile data could look like following. The exact contents depend on
 * how the application is provisioned at the server side and which scope 
 * is asked in the access token.
 *
 * <pre class="prettyprint">
 * "tmobileid" : "45730458390eflkjsdf",
 * "TMO_ID_profile": 
 * {
 *     "firstName": "Jane",
 *     "lastName": "Doe",
 *     "email": janedoe@example.com,
 *     "userName" :"bobyjoe",
 *     "impu1" : "sip:bobyjoe@tmobileid.com",
 *     "openId" : "bobyjoe@tmobileid.com",
 *     "origTokenId" :"1234567890901223345561123234454",
 *     "accountCreationDate" : "1354595842",
 *     "isTMO" : "TRUE",
 *     ….
 * }
 * "associated_lines":
 * {
 *     "defaultTMOAccount" : "line1",
 *     "lines" : 
 *     [
 *         {
 *             "lineId" : "line1",
 *             "isGBA" : "TRUE",
 *             "GBABTID" : "34uwfrjfewoureorwe",
 *             "MSISDN" : "tel:+15555555555",
 *             "customerType" : "postpaid",
 *             "accountStatus" : "active",
 *             "customerId" : "423523525",
 *             "BAN" : "3432050"
 *         },
 *         {
 *             "lineId" : "line2",
 *             "isGBA" : "FALSE",
 *             "GBABTID" : "",
 *             "MSISDN" : "tel:+15555555556",
 *             "customerType" : "prepaid",
 *             "accountStatus" : "active",
 *             "customerId" : "23794230",
 *             "BAN" : "3432050",
 *         }
 *     ]
 * }
 * "billing_information":
 * {
 *     "billingPreference" : "method1",
 *     "methods" : 
 *     [
 *         {
 *             "methodId" : "method1",
 *             "paymentType" : "TMO_Direct",
 *             "paymentAccount" : "tel:+15555555555"
 *         },
 *         {
 *             "methodId" : "method2",
 *             "paymentType" : "PayPal",
 *             "paymentAccount" : "xxx"
 *         }
 *     ]
 * }
 * "entitlements":
 * [
 *     {
 *         "entitlementName" : "{entitlement name}",
 *         "entitlementEval": "TRUE",
 *         "entitlementDesc" : "Subscriber entitled for Offer XYZ"
 *     },
 *     {
 *         "entitlementName" : "{entitlement name}",
 *         "entitlementEval": "FALSE",
 *         "entitlementDesc" : "Subscriber not entitled for Offer ABC"
 *     }
 * ]
 * </pre>
 *
 * <p>So, the profile may contain five sections:</p>
 * <ul>
 * <li>tmobileid: anonymous, hashed id. Can be tested for equality, but does not reveal the user</li>
 * <li>TMO_ID_profile: User's profile</li>
 * <li>associated_lines: Different phone numbers of the user</li>
 * <li>billing_information</li>
 * <li>entitlements</li>
 * </ul>
 *
 * @author mihai.popoaei@movial.com
 */
public class ConsumerProfileIPC extends JSONObject implements Parcelable {
    Throwable agent_exception;

    public ConsumerProfileIPC() {
    }

    public ConsumerProfileIPC(Parcel in) {
        readFromParcel(in);
    }

    public Throwable getAgent_exception() {
        return agent_exception;
    }

    public void setAgent_exception(Throwable agent_exception) {
        this.agent_exception = agent_exception;
    }

    public boolean hasErrors() {
        return agent_exception != null;
    }

    public String jsonAsString() {
        return super.toString();
    }

    @Override
    public String toString() {
        return "ConsumerProfileIPC{" +
                "json={" + super.toString() + "}," +
                "agent_exception=" + agent_exception +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public void copyFrom(JSONObject source) {
        Iterator it = source.keys();
        while (it.hasNext()) {
            String name = (String) it.next();
            try {
                put(name, source.get(name));
            } catch (JSONException e) {
            }
        }
    }

    public void readFromParcel(Parcel p) {
        try {
            JSONObject other = new JSONObject(p.readString());
            copyFrom(other);

            int agent_exception_bytearray_length = p.readInt();
            if (agent_exception_bytearray_length != 0) {
                byte[] buffer = new byte[agent_exception_bytearray_length];
                try {
                    p.readByteArray(buffer);
                    ObjectInputStream inputStream = new ObjectInputStream(new ByteArrayInputStream(buffer));
                    agent_exception = (Throwable) inputStream.readObject();
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(super.toString());

        if (agent_exception != null) {
            // serialize throwable object
            ByteArrayOutputStream arrayStream = new ByteArrayOutputStream();
            try {
                ObjectOutputStream objectStream = new ObjectOutputStream(arrayStream);
                objectStream.writeObject(agent_exception);
                objectStream.close();

                byte[] byteArray = arrayStream.toByteArray();
                // first write byte array length
                dest.writeInt(byteArray.length);
                dest.writeByteArray(byteArray);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        } else {
            dest.writeInt(0);
        }
    }

    public static final Parcelable.Creator<ConsumerProfileIPC> CREATOR = new
            Parcelable.Creator<ConsumerProfileIPC>() {
                public ConsumerProfileIPC createFromParcel(Parcel in) {
                    return new ConsumerProfileIPC(in);
                }

                public ConsumerProfileIPC[] newArray(int size) {
                    return new ConsumerProfileIPC[size];
                }
            };

}

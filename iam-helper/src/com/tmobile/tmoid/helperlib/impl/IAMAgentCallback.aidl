package com.tmobile.tmoid.helperlib.impl;

import com.tmobile.tmoid.helperlib.impl.APIResponse;

/**
 * FIXME: Seems to be unused
 *
 * {@hide}
 */
interface IAMAgentCallback {
    void callback(in APIResponse result);
}

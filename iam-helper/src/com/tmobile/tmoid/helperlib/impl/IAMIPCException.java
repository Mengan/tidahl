package com.tmobile.tmoid.helperlib.impl;

import com.tmobile.tmoid.helperlib.IAMException;

/**
 * This seems to be completely unused.
 * {@hide}
 * @author mihai.popoaei@movial.com
 */
public class IAMIPCException extends IAMException {
    public IAMIPCException(Exception e) {
        super(e);
    }
}

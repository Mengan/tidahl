package com.tmobile.tmoid.helperlib.impl;

import android.content.Intent;
import android.os.Parcel;
import android.os.Parcelable;
import com.tmobile.tmoid.helperlib.Agent;

import java.util.UUID;

/**
 * TODO: This class is internal IPC-class used in AIDL-calls. We need
 * to revisit the usefulness of ACCESS_TYPE and DISPLAY properties for
 * public API. I'll currently hide this from public API.
 *
 * {@hide}
 * @author mihai.popoaei@movial.com
 */
public abstract class APIRequest implements Parcelable {
    protected String id = UUID.randomUUID().toString();
    protected String action;

    protected String client_id;

    protected APIResponse response;

    @Override
    public String toString() {
        return new StringBuilder()
                .append("request{id: ").append(id)
                .append(", action:").append(action)
                .append(", client_id:").append(client_id)
                .toString();
    }

    public static APIRequest fromIntent(Intent intent) {
        String action = intent.getStringExtra("action");

        if ("getAuthorizationCode".equals(action)) {
            return new APIRequestForAuthorizationCode(intent);
        } else if ("getAccessToken".equals(action)) {
            return new APIRequestForAccessToken(intent);
        }

        return new EmptyAPIRequest();
    }

    /**
     * FIXME: Client ID is not leaked from the device agent
     * {@hide}
     */
    public String getClient_id() {
        return client_id;
    }

    /**
     * FIXME: Client cannot provide Id as parameter. Device agent fetches it from environment
     * {@hide}
     */
    public void setClient_id(String client_id) {
        this.client_id = client_id;
    }

    public String getAction() {
        return action;
    }

    public APIResponse getResponse() {
        return response;
    }

    public void setResponse(APIResponse response) {
        this.response = response;
    }

    public String getId() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof APIRequest)) return false;

        APIRequest that = (APIRequest) o;

        if (id != null ? !id.equals(that.id) : that.id != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    public static class EmptyAPIRequest extends APIRequest {

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {

        }
    }
}

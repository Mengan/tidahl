package com.tmobile.tmoid.helperlib.impl;

import com.tmobile.tmoid.helperlib.impl.APIResponse;
import com.tmobile.tmoid.helperlib.impl.APIRequestForAccessToken;
import com.tmobile.tmoid.helperlib.impl.APIRequestForAuthorizationCode;
import com.tmobile.tmoid.helperlib.impl.IAMAgentCallback;
import com.tmobile.tmoid.helperlib.impl.ConsumerProfileIPC;

/**
 * @author mihai.popoaei@movial.com
 * {@hide}
 */
interface IHelperLibrary {
    boolean isAuthenticated();
    String getAuthenticatedUserIdentifier();

    APIResponse getAuthorizationCode(in APIRequestForAuthorizationCode request);
    APIResponse getAccessToken(in APIRequestForAccessToken request);
    ConsumerProfileIPC getProfileInformation(String accessToken);
}

package com.tmobile.tmoid.helperlib.impl;

import java.util.Vector;

/**
* Author: mures.andone@movial.com
* {@hide}
*/
public class FixedStringList extends Vector<String> {
    int maxSize;

    public FixedStringList(int maxSize) {
        this.maxSize = maxSize;
    }

    public void addLine(String line) {
        if(line != null) {
            this.add(line.trim());
            if(this.size() >= maxSize)
                this.removeElementAt(0);
        }
    }

    public synchronized String toString() {
        String res="";
        synchronized (this) {
            for(int i=0; i<this.size(); i++) {
                res += this.elementAt(i) + "\n";
            }
        }
        return res;
    }
}

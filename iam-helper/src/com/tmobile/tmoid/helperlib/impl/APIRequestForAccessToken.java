package com.tmobile.tmoid.helperlib.impl;

import android.content.Intent;
import android.os.Parcel;
import android.os.Parcelable;

/**
 */
public class APIRequestForAccessToken extends APIRequest implements Parcelable{
    private String client_secret;
    private String authorizationCode;
    private String packageName = "";

    public APIRequestForAccessToken(String client_id, String client_secret, String authorizationCode) {
        this.client_id = client_id;
        this.client_secret = client_secret;
        this.authorizationCode = authorizationCode;

        this.action = "getAccessToken";
    }

    public APIRequestForAccessToken(Intent intent) {
        id = intent.getStringExtra("request_id");
        action = intent.getStringExtra("action");
        client_id = intent.getStringExtra("client_id");
        client_secret = intent.getStringExtra("client_secret");

        authorizationCode = intent.getStringExtra("authorization_code");
        packageName = intent.getStringExtra("package_name");
    }

    public String getAuthorizationCode() {
        return authorizationCode;
    }

    public void setAuthorizationCode(String authorizationCode) {
        this.authorizationCode = authorizationCode;
    }

    /**
     * FIXME: Client secret is not leaked from the device agent. This needs to be removed
     * {@hide}
     */
    public String getClient_secret() {
        return client_secret;
    }

    /**
     * FIXME: Client cannot provide secret as parameter. Device agent fetches it form environment
     * {@hide}
     */
    public void setClient_secret(String client_secret) {
        this.client_secret = client_secret;
    }

    /**
     * FIXME: Client package name is not leaked from the device agent. This needs to be removed
     * {@hide}
     */
    public String getPackageName() {
        return packageName;
    }

    /**
     * FIXME: Client cannot provide package name as parameter. Device agent fetches it form environment
     * {@hide}
     */
    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public static final Parcelable.Creator<APIRequestForAccessToken> CREATOR = new
            Parcelable.Creator<APIRequestForAccessToken>() {
                public APIRequestForAccessToken createFromParcel(Parcel in) {
                    return new APIRequestForAccessToken(in);
                }

                public APIRequestForAccessToken[] newArray(int size) {
                    return new APIRequestForAccessToken[size];
                }
            };

    private APIRequestForAccessToken(Parcel in) {
        readFromParcel(in);
    }

    public void readFromParcel(Parcel in) {
        client_id = in.readString();
        client_secret = in.readString();
        authorizationCode = in.readString();
        id = in.readString();
        action = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(client_id);
        dest.writeString(client_secret);
        dest.writeString(authorizationCode);
        dest.writeString(id);
        dest.writeString(action);
    }

    @Override
    public String toString() {
        return new StringBuilder()
                .append("request{id: ").append(id)
                .append(", action:").append(action)
                .append(", client_id:").append(client_id)
                .append(", package_name:").append(packageName)
                .append(", authorization_code:").append(authorizationCode)
                .append("}")
                .toString();
    }
}

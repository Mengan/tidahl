package com.tmobile.tmoid.helperlib.impl;

import android.os.RemoteException;
import com.tmobile.tmoid.helperlib.util.Log;
import com.tmobile.tmoid.helperlib.*;

import org.json.JSONException;

/**
 * @author mihai.popoaei@movial.com
 * {@hide}
 */
public class AgentImpl extends Agent {
    // Moved this constant to impl package to make it disappear from the public docs
    public static String _E_HARDCODED_CLIENT_ID_TO_BE_REMOVED = "A-jOgGd14-iz0";
    public static String _E_HARDCODED_CLIENT_SECRET_TO_BE_REMOVED = "329ufjwf9ops";
    public static String _E_HARDCODED_DEVICE_AGENT = "IAM_Device_Agent/1.0 Android/4.2";

    IHelperLibrary helper_library;

    static class Defaults {
        static String client_id = AgentImpl._E_HARDCODED_CLIENT_ID_TO_BE_REMOVED;
        static String client_secret = AgentImpl._E_HARDCODED_CLIENT_SECRET_TO_BE_REMOVED;
        static String scope = Agent.SCOPE_TMO_ID_PROFILE;
        static String access_type = Agent.ACCESS_TYPE_ONLINE;
        static String display = Agent.DISPLAY_PAGE;
        static String reauth = Agent.REAUTH_AUTO;
    }

    public AgentImpl(IHelperLibrary helper_library) {
        this.helper_library = helper_library;
    }

    @Override
    public boolean requestAuthenticationStatus() throws CommunicationException {
        try {
            return helper_library.isAuthenticated();
        } catch (RemoteException e) {
            throw new CommunicationException(e);
        }
    }

    @Override
    public String requestUserIdentifier() throws CommunicationException {
        try {
            return helper_library.getAuthenticatedUserIdentifier();
        } catch (RemoteException e) {
            throw new CommunicationException(e);
        }
    }

    @Override
    public String requestAuthorizationCode(String client_id, String scope,
            String access_type, String display, String reauth, String approval_prompt)
            throws RequestCanceledException, CommunicationException, ServerErrorException, InvalidStateException {

        APIRequestForAuthorizationCode request = APIRequestForAuthorizationCode.
                createRequest(client_id, scope, access_type, display, reauth, approval_prompt);

        try {
            APIResponse response = helper_library.getAuthorizationCode(request);

            if (response == null) {
                throw new IllegalStateException("Received DeviceAgent null response (requestAccessToken)");
            }
            else if (response.isRequestHasBeenCanceled()) {
                throw new RequestCanceledException();
            }
            else if (response.hasErrors()) {
                Throwable thr = response.getAgent_exception();
                if (thr instanceof ServerErrorException) {
                    throw (ServerErrorException) thr;
                } else if (thr instanceof InvalidStateException) {
                    throw (InvalidStateException) thr;
                } else {
                    throw new IllegalStateException("unexpected");
                }
            }

            return response.getAuthorization_code();
        } catch (RemoteException e) {
            throw new CommunicationException(e);
        }
    }

    @Override
    public AccessToken requestAccessToken(
            String client_id, String client_secret, String authorization_code)
            throws CommunicationException, ServerErrorException, RequestCanceledException  {
        try {
            APIResponse response = helper_library.getAccessToken(
                    new APIRequestForAccessToken(client_id, client_secret, authorization_code));
            if (response == null) {
                throw new IllegalStateException("Received DeviceAgent null response (requestAccessToken)");
            }
            else if (response.isRequestHasBeenCanceled()) {
                throw new RequestCanceledException();
            }
            else if (response.hasErrors()) {
                Throwable thr = response.getAgent_exception();
                if (thr instanceof ServerErrorException) {
                    throw (ServerErrorException) thr;
                } else {
                    throw new IllegalStateException("unexpected");
                }
            }
            return response.toAccessToken();
        } catch (RemoteException e) {
            throw new CommunicationException(e);
        }
    }

    @Override
    public AccessToken refreshAccessToken(String refresh_token, String scope) {
        return null;
    }

    @Override
    public AccessToken refreshAccessToken(String refresh_token) {
        return null;
    }

    @Override
    public ConsumerProfile requestConsumerProfile(AccessToken access_token) throws AccessTokenExpiredException, ServerErrorException, CommunicationException {
        if (access_token == null) {
            throw new IllegalArgumentException("Access token is null");
        }

        if (access_token.isExpired()) {
            throw new AccessTokenExpiredException();
        }

        try {
            ConsumerProfileIPC profileInfo = helper_library.getProfileInformation(access_token.getToken());
            if (profileInfo == null) {
                throw new ServerErrorException(ServerErrorException.AGENT_ERROR_GENERIC_ERROR);
            } else if (profileInfo.hasErrors()) {
                Throwable thr = profileInfo.getAgent_exception();
                if (thr instanceof ServerErrorException) {
                    throw (ServerErrorException) thr;
                } else if (thr instanceof AccessTokenExpiredException) {
                    throw (AccessTokenExpiredException) thr;
                }
            }

            // @todo review, we should use another method to copy from ConsumerProfileIPC
            return new ConsumerProfile(profileInfo.jsonAsString());
        } catch (JSONException e) {
            // never code
            throw new ServerErrorException(ServerErrorException.AGENT_ERROR_JSON_DECODING, e);
        } catch (RemoteException e) {
            throw new CommunicationException(e);
        }
    }
}

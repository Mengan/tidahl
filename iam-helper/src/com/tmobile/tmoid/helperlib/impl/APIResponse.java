package com.tmobile.tmoid.helperlib.impl;

import android.os.Parcel;
import android.os.Parcelable;
import com.tmobile.tmoid.helperlib.AccessToken;
import com.tmobile.tmoid.helperlib.ServerErrorException;
import com.tmobile.tmoid.helperlib.util.ChainedStringWriter;

import java.io.*;

/**
 * TODO: This is an internal class used by helper library to communicate
 * with the device agent. We need to review the usefullness of these fields
 * and probably to introduce a limited AccessToken class to appear in the
 * public API instead.
 * 
 * @author mihai.popoaei@movial.com
 */
public class APIResponse implements Parcelable {
    String access_token;
    String refresh_token;
    String token_type;
    int expires_in;
    String tmobileid;
    String scope;
    String authorization_code;
    long date;

    boolean requestHasBeenCanceled = false;

    Throwable agent_exception;

    @Override
    public String toString() {
        ChainedStringWriter csw = new ChainedStringWriter();

        csw.print("{");
        csw.print("access_token:").print(access_token).print(",");
        csw.print("refresh_token:").print(refresh_token).print(",");
        csw.print("token_type:").print(token_type).print(",");
        csw.print("expires_in:").print(expires_in).print(",");
        csw.print("tmobileid:").print(tmobileid).print(",");
        csw.print("scope:").print(scope).print(",");
        csw.print("date:").print(date).print(",");
        csw.print("authorization_code:").print(authorization_code).print(",");
        csw.print("requestHasBeenCanceled:").print(requestHasBeenCanceled).print(",");
        csw.print("requestHasServerError:").print(agent_exception != null).print(",");

        if (agent_exception != null) {
            csw.println("agent_exception:");
            agent_exception.printStackTrace(csw.getWriter());
        }

        csw.print("}");

        return csw.getString();
    }

    public APIResponse() {
    }

    public APIResponse(Parcel in) {
        readFromParcel(in);
    }

    public Throwable getAgent_exception() {
        return agent_exception;
    }

    public void setAgent_exception(Throwable agent_exception) {
        this.agent_exception = agent_exception;
    }

    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }

    public String getRefresh_token() {
        return refresh_token;
    }

    public void setRefresh_token(String refresh_token) {
        this.refresh_token = refresh_token;
    }

    public String getToken_type() {
        return token_type;
    }

    public void setToken_type(String token_type) {
        this.token_type = token_type;
    }

    /**
     * seconds
     *
     * @return
     */
    public int getExpires_in() {
        return expires_in;
    }

    public void setExpires_in(int expires_in) {
        this.expires_in = expires_in;
    }

    public String getTmobileid() {
        return tmobileid;
    }

    public void setTmobileid(String tmobileid) {
        this.tmobileid = tmobileid;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public String getAuthorization_code() {
        return authorization_code;
    }

    public void setAuthorization_code(String authorization_code) {
        this.authorization_code = authorization_code;
    }

    public boolean isRequestHasBeenCanceled() {
        return requestHasBeenCanceled;
    }

    public void setRequestHasBeenCanceled(boolean requestHasBeenCanceled) {
        this.requestHasBeenCanceled = requestHasBeenCanceled;
    }

    public boolean hasErrors() {
        return agent_exception != null;
    }

    public AccessToken toAccessToken() {
        return new AccessToken(access_token, expires_in, tmobileid, scope, date, refresh_token, token_type);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(access_token);
        dest.writeString(refresh_token);
        dest.writeString(token_type);
        dest.writeInt(expires_in);
        dest.writeString(tmobileid);
        dest.writeString(scope);
        dest.writeLong(date);
        dest.writeInt(requestHasBeenCanceled ? 1 : 0);
        dest.writeString(authorization_code);

        if (agent_exception != null) {
            // serialize throwable object
            ByteArrayOutputStream arrayStream = new ByteArrayOutputStream();
            try {
                ObjectOutputStream objectStream = new ObjectOutputStream(arrayStream);
                try {
                    objectStream.writeObject(agent_exception);
                } catch (NotSerializableException e) {
                    // caught an exception that is not serializable
                    // send fallback server exception
                    ServerErrorException fallbackException = new ServerErrorException(ServerErrorException.ERROR_UNKNOWN);
                    fallbackException.setError(agent_exception.getMessage());
                    objectStream.writeObject(fallbackException);
                }
                objectStream.close();

                byte[] byteArray = arrayStream.toByteArray();
                // first write byte array length
                dest.writeInt(byteArray.length);
                dest.writeByteArray(byteArray);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        } else {
            dest.writeInt(0);
        }
    }

    public void readFromParcel(Parcel p) {
        access_token = p.readString();
        refresh_token = p.readString();
        token_type = p.readString();
        expires_in = p.readInt();
        tmobileid = p.readString();
        scope = p.readString();
        date = p.readLong();
        requestHasBeenCanceled = p.readInt() == 1;
        authorization_code = p.readString();

        int agent_exception_bytearray_length = p.readInt();
        if (agent_exception_bytearray_length != 0) {
            byte[] buffer = new byte[agent_exception_bytearray_length];
            try {
                p.readByteArray(buffer);
                ObjectInputStream inputStream = new ObjectInputStream(new ByteArrayInputStream(buffer));
                try {
                    agent_exception = (Throwable) inputStream.readObject();
                } catch (WriteAbortedException e) {
                    // agent tried to send an exception that was not serializable
                    // fetch fallback exception
                    agent_exception = (Throwable) inputStream.readObject();
                }
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }

    public static final Parcelable.Creator<APIResponse> CREATOR = new
            Parcelable.Creator<APIResponse>() {
                public APIResponse createFromParcel(Parcel in) {
                    return new APIResponse(in);
                }

                public APIResponse[] newArray(int size) {
                    return new APIResponse[size];
                }
            };

}

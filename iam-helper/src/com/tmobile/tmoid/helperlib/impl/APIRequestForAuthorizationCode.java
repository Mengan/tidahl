package com.tmobile.tmoid.helperlib.impl;

import android.content.Intent;
import android.os.Parcel;
import android.os.Parcelable;
import com.tmobile.tmoid.helperlib.Agent;

/**
 */
public class APIRequestForAuthorizationCode  extends APIRequest implements Parcelable{
    private String access_type = Agent.ACCESS_TYPE_ONLINE;
    private String display = Agent.DISPLAY_PAGE;
    private String reauth = Agent.REAUTH_AUTO;
    private String approval_prompt = Agent.APPROVAL_PROMPT_AUTO;

    private String scope;

    protected APIRequestForAuthorizationCode(Intent intent) {
        id = intent.getStringExtra("request_id");
        action = intent.getStringExtra("action");
        scope = intent.getStringExtra("scope");
        client_id = intent.getStringExtra("client_id");
        access_type = intent.getStringExtra("access_type");
        display = intent.getStringExtra("display");
        reauth = intent.getStringExtra("reauth");
        approval_prompt = intent.getStringExtra("approval_prompt");
    }

    private APIRequestForAuthorizationCode(String client_id, String scope, String access_type, String display, String reauth, String approval_prompt) {
        this.client_id = client_id;
        this.scope = scope;
        this.access_type = access_type;
        this.display = display;
        this.reauth = reauth;
        this.approval_prompt = approval_prompt;

        this.action = "getAuthorizationCode";
    }

    private APIRequestForAuthorizationCode(String client_id, String client_scope) {
        this(client_id, client_scope, Agent.ACCESS_TYPE_ONLINE, Agent.DISPLAY_PAGE, Agent.REAUTH_AUTO, Agent.APPROVAL_PROMPT_AUTO);
    }

    private APIRequestForAuthorizationCode(String client_id, String client_scope, String access_type) {
        this(client_id, client_scope, access_type, Agent.DISPLAY_PAGE, Agent.REAUTH_AUTO, Agent.APPROVAL_PROMPT_AUTO);
    }

    private APIRequestForAuthorizationCode(String client_id, String client_scope, String access_type, String display) {
        this(client_id, client_scope, access_type, display, Agent.REAUTH_AUTO, Agent.APPROVAL_PROMPT_AUTO);
    }

    private APIRequestForAuthorizationCode(String client_id, String client_scope, String access_type, String display, String reauth) {
        this(client_id, client_scope, access_type, display, reauth, Agent.APPROVAL_PROMPT_AUTO);
    }

    public static APIRequestForAuthorizationCode createRequest(String client_id, String scope) {
        return new APIRequestForAuthorizationCode(client_id, scope);
    }

    public static APIRequestForAuthorizationCode createRequest(String client_id, String scope, String access_type) {
        return new APIRequestForAuthorizationCode(client_id, scope, access_type);
    }

    public static APIRequestForAuthorizationCode createRequest(String client_id, String scope, String access_type,
                                                               String display) {
        return new APIRequestForAuthorizationCode(client_id, scope, access_type, display);
    }

    public static APIRequestForAuthorizationCode createRequest(String client_id, String scope, String access_type,
                                                               String display, String reauth) {
        return new APIRequestForAuthorizationCode(client_id, scope, access_type, display, reauth);
    }

    public static APIRequestForAuthorizationCode createRequest(String client_id, String scope, String access_type,
                                                               String display, String reauth, String approval_prompt) {
        return new APIRequestForAuthorizationCode(client_id, scope, access_type, display, reauth, approval_prompt);
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public String getAccess_type() {
        return access_type;
    }

    public void setAccess_type(String access_type) {
        this.access_type = access_type;
    }

    public String getDisplay() {
        return display;
    }

    public void setDisplay(String display) {
        this.display = display;
    }

    public String getReauth() {
        return reauth;
    }

    public void setReauth(String reauth) {
        this.reauth = reauth;
    }

    public String getApprovalPrompt() {
        return approval_prompt;
    }

    public void setApprovalPrompt(String approval_prompt) {
        this.approval_prompt = approval_prompt;
    }

    public static final Parcelable.Creator<APIRequestForAuthorizationCode> CREATOR = new
            Parcelable.Creator<APIRequestForAuthorizationCode>() {
                public APIRequestForAuthorizationCode createFromParcel(Parcel in) {
                    return new APIRequestForAuthorizationCode(in);
                }

                public APIRequestForAuthorizationCode[] newArray(int size) {
                    return new APIRequestForAuthorizationCode[size];
                }
            };

    private APIRequestForAuthorizationCode(Parcel in) {
        readFromParcel(in);
    }

    public void readFromParcel(Parcel in) {
        client_id = in.readString();
        scope = in.readString();
        id = in.readString();
        action = in.readString();
        access_type = in.readString();
        display = in.readString();
        reauth = in.readString();
        approval_prompt = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(client_id);
        dest.writeString(scope);
        dest.writeString(id);
        dest.writeString(action);
        dest.writeString(access_type);
        dest.writeString(display);
        dest.writeString(reauth);
        dest.writeString(approval_prompt);
    }

    @Override
    public String toString() {
        return new StringBuilder()
                .append("request{id: ").append(id)
                .append(", action:").append(action)
                .append(", client_id:").append(client_id)
                .append(", access_type:").append(access_type)
                .append(", display:").append(display)
                .append(", reauth:").append(reauth)
                .append(", approval_prompt:").append(approval_prompt)
                .append("}")
                .toString();
    }
}

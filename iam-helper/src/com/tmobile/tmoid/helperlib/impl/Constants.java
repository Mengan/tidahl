package com.tmobile.tmoid.helperlib.impl;

import android.os.Environment;

import java.io.File;

/**
 * Internal constant declarations for helper library.
 *
 * {@hide}
 * Author: mures.andone@movial.com
 */
public class Constants {
    public static final boolean DEBUG = true;

    public static final String AGENT_PACKAGE_NAME = "com.tmobile.tmoid.agent";
    public static final String LOG_TAG = "TMO-Helperlib";
    public static final String LOG_FILTER_STRING = "TMO-";
    public static final int MAX_LOG_LINES = 32000;
    public static final String LOCAL_SOCKET_ADDRESS = "com.movial.tmo.iamlogger.localsocket";
    public static final String SDCARD_LOG_FILE = Environment.getExternalStorageDirectory().getAbsolutePath()
            + File.separator + "iam-log.txt";

    public static final String ACCOUNT_TYPE = "com.tmobile.tmoid.accounttype";
    public static final String AUTHTOKEN_TYPE = "com.tmobile.tmoid.accounttype";
    public static final String ACCOUNT_AUTHORITY = "com.tmobile.tmoid.provider";
    public static final String AUTHTOKEN_LABEL = "IAMTMOAccount";

}

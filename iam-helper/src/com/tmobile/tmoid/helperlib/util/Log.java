package com.tmobile.tmoid.helperlib.util;

import android.content.*;
import android.content.pm.PackageManager;
import android.os.*;
import com.tmobile.tmoid.helperlib.impl.Constants;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

public class Log {
    public static final int OUTPUT_LOGS = 1;
    public static final int SUPPRESS_LOGS = 2;
    public static final int DISPLAY_LOG = 3;
    public static final int REGISTER_CLIENT = 4;

    private static String LOG_TAG;

    private static Context context = null;
    private static ServiceConnection mConnection = new LoggingServiceConnection();
    // Messenger for communicating with the service.
    static Messenger mService = null;
    // Flag indicating whether we have called bind on the service.
    private static boolean mBound = false;
    private static boolean shouldOutputLog = false;
    private static LoggingMode loggingMode = LoggingMode.DO_NOT_LOG;

    private Log() {}

    public static synchronized void init(Context ctx, LoggingMode mode, String appName) {
        PackageManager packageManager = ctx.getPackageManager();
        try {
            packageManager.getPackageInfo("com.tmobile.tmoid.iamlogger", PackageManager.GET_ACTIVITIES);
        } catch (PackageManager.NameNotFoundException ignored) {
            return;
        }

        if (!mBound && mode != LoggingMode.DO_NOT_LOG) {
            context = ctx;
            loggingMode = mode;
            LOG_TAG = appName;

            // Bind to the service
            Intent intent = new Intent("IAMLoggerService");
            // support for API_LEVEL >= 21 (LOLLIPOP)
            intent.setPackage("com.tmobile.tmoid.iamlogger");
            context.bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
        }
    }

    private static void sendViaIPC(char level, String tag, String msg, Throwable tr) {
        Message message = Message.obtain();
        message.what = DISPLAY_LOG;
        Bundle data = new Bundle();
        data.putChar("level", level);
        data.putString("tag", tag);
        data.putString("msg", msg);

        if (tr != null) {
            // serialize throwable object
            ByteArrayOutputStream arrayStream = new ByteArrayOutputStream();
            try {
                ObjectOutputStream objectStream = new ObjectOutputStream(arrayStream);
                objectStream.writeObject(tr);
                objectStream.close();

                data.putByteArray("error", arrayStream.toByteArray());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        message.setData(data);
        try {
            mService.send(message);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    public static void stop() {
        if (mBound) {
            context.unbindService(mConnection);
            mBound = false;
        }
    }

    private static void printLog(char level, String tag, String msg, Throwable tr) {
        if (loggingMode == LoggingMode.REMOTE_VIA_IPC) {
            sendViaIPC(level, tag, msg, tr);
        } else {
            switch (level) {
                case 'V':
                    android.util.Log.v(tag, msg, tr);
                    break;
                case 'D':
                    android.util.Log.d(tag, msg, tr);
                    break;
                case 'I':
                    android.util.Log.i(tag, msg, tr);
                    break;
                case 'W':
                    android.util.Log.w(tag, msg, tr);
                    break;
                case 'E':
                    android.util.Log.e(tag, msg, tr);
                    break;
            }
        }
    }

    /**
     * Send a VERBOSE log message and log the exception.
     * <p>
     * VERBOSE logs are REMOVED FROM PRODUCTION BUILDS
     *
     * @param tag Used to identify the source of a log message. It usually identifies the class or activity where the log call occurs.
     * @param msg The message you would like logged.
     * @param tr An exception to log
     */
    public static void v(String tag, String msg, Throwable tr) {
        if (shouldOutputLog)
            printLog('V', tag, msg, tr);
    }

    /**
     * Send a VERBOSE log message.
     * <p>
     * VERBOSE logs are REMOVED FROM PRODUCTION BUILDS
     *
     * @param tag Used to identify the source of a log message. It usually identifies the class or activity where the log call occurs.
     * @param msg The message you would like logged.
     */
    public static void v(String tag, String msg) {
        v(tag, msg, null);
    }

    /**
     * Send a DEBUG log message and log the exception.
     * <p>
     * DEBUG logs are KEPT IN PRODUCTION BUILDS
     *
     * @param tag Used to identify the source of a log message. It usually identifies the class or activity where the log call occurs.
     * @param msg The message you would like logged.
     * @param tr An exception to log
     */
    public static void d(String tag, String msg, Throwable tr) {
        if (shouldOutputLog)
            printLog('D', tag, msg, tr);
    }

    /**
     * Send a DEBUG log message.
     * <p>
     * DEBUG logs are KEPT IN PRODUCTION BUILDS
     *
     * @param tag Used to identify the source of a log message. It usually identifies the class or activity where the log call occurs.
     * @param msg The message you would like logged.
     */
    public static void d(String tag, String msg) {
        d(tag, msg, null);
    }

    /**
     * Send an INFO log message and log the exception.
     * <p>
     * INFO logs are KEPT IN PRODUCTION BUILDS
     *
     * @param tag Used to identify the source of a log message. It usually identifies the class or activity where the log call occurs.
     * @param msg The message you would like logged.
     * @param tr An exception to log
     */
    public static void i(String tag, String msg, Throwable tr) {
        if (shouldOutputLog)
            printLog('I', tag, msg, tr);
    }

    /**
     * Send an INFO log message.
     * <p>
     * INFO logs are KEPT IN PRODUCTION BUILDS
     *
     * @param tag Used to identify the source of a log message. It usually identifies the class or activity where the log call occurs.
     * @param msg The message you would like logged.
     */
    public static void i(String tag, String msg) {
        i(tag, msg, null);
    }

    /**
     * Send a WARNING log message and log the exception.
     * <p>
     * WARNING logs are KEPT IN PRODUCTION BUILDS
     *
     * @param tag Used to identify the source of a log message. It usually identifies the class or activity where the log call occurs.
     * @param msg The message you would like logged.
     * @param tr An exception to log
     */
    public static void w(String tag, String msg, Throwable tr) {
        if (shouldOutputLog)
            printLog('W', tag, msg, tr);
    }

    /**
     * Send a WARNING log message and log the exception.
     * <p>
     * WARNING logs are KEPT IN PRODUCTION BUILDS
     *
     * @param tag Used to identify the source of a log message. It usually identifies the class or activity where the log call occurs.
     * @param msg The message you would like logged.
     */
    public static void w(String tag, String msg) {
        w(tag, msg, null);
    }

    /**
     * Send an ERROR log message and log the exception.
     * <p>
     * ERROR logs are KEPT IN PRODUCTION BUILDS
     *
     * @param tag Used to identify the source of a log message. It usually identifies the class or activity where the log call occurs.
     * @param msg The message you would like logged.
     * @param tr An exception to log
     */
    public static void e(String tag, String msg, Throwable tr) {
        if (shouldOutputLog)
            printLog('E', tag, msg, tr);
    }

    /**
     * Send an ERROR log message.
     * <p>
     * ERROR logs are KEPT IN PRODUCTION BUILDS
     *
     * @param tag Used to identify the source of a log message. It usually identifies the class or activity where the log call occurs.
     * @param msg The message you would like logged.
     */
    public static void e(String tag, String msg) {
        e(tag, msg, null);
    }

    public enum LoggingMode {
        PRINT_TO_LOG, REMOTE_VIA_IPC, DO_NOT_LOG
    }

    private static class LoggingServiceConnection implements ServiceConnection {
        Messenger serviceResponseHandler = new Messenger(new Handler() {

            @Override
            public void handleMessage(Message msg) {
                // handle service message for client
                switch (msg.what) {
                    case OUTPUT_LOGS:
                        shouldOutputLog = true;
                        break;
                    case SUPPRESS_LOGS:
                        shouldOutputLog = false;
                        break;
                }

                Log.d(LOG_TAG, "logging state changed: shouldOutputLog is " + shouldOutputLog);

                super.handleMessage(msg);
            }
        });

        @Override
        public void onServiceConnected(ComponentName className, IBinder service) {
            mService = new Messenger(service);
            mBound = true;

            loggingMode = LoggingMode.REMOTE_VIA_IPC;

            Message registerClient = Message.obtain();
            registerClient.what = REGISTER_CLIENT;
            registerClient.replyTo = serviceResponseHandler;

            Bundle extra = new Bundle();
            extra.putString("app_name", LOG_TAG);
            registerClient.setData(extra);

            try {
                mService.send(registerClient);
            } catch (RemoteException e) {
                Log.e(Constants.LOG_TAG, "exception while registering to logger", e);
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName className) {
            mService = null;
            mBound = false;

            shouldOutputLog = false;
        }
    }
}

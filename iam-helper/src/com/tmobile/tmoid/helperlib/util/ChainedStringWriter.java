package com.tmobile.tmoid.helperlib.util;

import java.io.*;

/**
 * @author mihai.popoaei@movial.com
 */
public class ChainedStringWriter {
    StringWriter sw = new StringWriter();
    PrintWriter writer = new PrintWriter(sw);

    public ChainedStringWriter print(String str) {
        writer.print(str);
        return this;
    }

    public ChainedStringWriter println(String str) {
        writer.println(str);
        return this;
    }

    public ChainedStringWriter print(int str) {
        writer.print(str);
        return this;
    }

    public ChainedStringWriter println(int str) {
        writer.println(str);
        return this;
    }

    public ChainedStringWriter print(boolean str) {
        writer.print(str);
        return this;
    }

    public ChainedStringWriter println(boolean str) {
        writer.println(str);
        return this;
    }

    public ChainedStringWriter print(long str) {
        writer.print(str);
        return this;
    }

    public ChainedStringWriter println(long str) {
        writer.println(str);
        return this;
    }

    public String getString() {
        writer.flush();
        return sw.toString();
    }

    public PrintWriter getWriter() {
        return writer;
    }
}

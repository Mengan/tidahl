package com.tmobile.tmoid.helperlib;

/**
 * Thrown by {@link AgentServiceConnection} in case
 * the IAM Device Agent application is not installed to the device.
 * The user is expected to be prompted to install device agent
 * from the platform's application store.
 *
 * <p>Application developer is usually not expected to create instances of this class directly.</p>
 */
public class AgentNotFoundException extends IAMException {
    public AgentNotFoundException() {
    }

    public AgentNotFoundException(Throwable throwable) {
        super(throwable);
    }
}

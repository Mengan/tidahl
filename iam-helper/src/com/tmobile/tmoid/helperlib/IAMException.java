package com.tmobile.tmoid.helperlib;

/**
 * Generic IAM-specific error.
 * <p>Application developer is usually not expected to create instances of this class directly.</p>
 */
public class IAMException extends Exception {
    public IAMException() {
    }

    public IAMException(String detailMessage) {
        super(detailMessage);
    }

    public IAMException(Throwable throwable) {
        super(throwable);
    }
}

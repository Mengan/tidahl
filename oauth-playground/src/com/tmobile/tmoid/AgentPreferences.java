package com.tmobile.tmoid;

import android.content.SharedPreferences;
import com.tmobile.tmoid.helperlib.AccessToken;

/**
 * This utility is rather thin, and contains expectations
 * how the using app would like to name the keys for preferences. Thus
 * hide it from API doc generation point of view.
 */
public class AgentPreferences {
    private AccessToken lastAccessToken;

    public boolean hasValidToken() {
        return lastAccessToken != null && !lastAccessToken.isExpired();
    }

    /**
     * <p>Saves last access token value to editor</p>
     * If the token is not valid (i.e. expired or NULL) removes previously saved values
     *
     * <p>Note: does not commit saved key values</p>
     *
     * @param editor
     */
    public void savePreferences(SharedPreferences.Editor editor) {
        if (!hasValidToken()) {
            editor.remove("access_token.access_token")
                    .remove("access_token.refresh_token")
                    .remove("access_token.scope")
                    .remove("access_token.tmobileid")
                    .remove("access_token.token_type")
                    .remove("access_token.expires_in")
                    .remove("access_token.create_time");

            return;
        }

        editor.putString("access_token.access_token", lastAccessToken.getToken());
        editor.putString("access_token.refresh_token", lastAccessToken.getRefreshToken());
        editor.putString("access_token.scope", lastAccessToken.getScope());
        editor.putString("access_token.tmobileid", lastAccessToken.getTmoId());
        editor.putString("access_token.token_type", lastAccessToken.getTokenType());
        editor.putInt("access_token.expires_in", lastAccessToken.getExpiresIn());
        editor.putLong("access_token.create_time", lastAccessToken.getTimeStamp());
    }

    public void load(SharedPreferences sp) {
        lastAccessToken = new AccessToken(
                sp.getString("access_token.access_token", null),
                sp.getInt("access_token.expires_in", -1),
                sp.getString("access_token.tmobileid", null),
                sp.getString("access_token.scope", null),
                sp.getLong("access_token.create_time", 0),
                sp.getString("access_token.refresh_token", null),
                sp.getString("access_token.token_type", null)
                );
    }

    public AccessToken getLastAccessToken() {
        return lastAccessToken;
    }

    public void setLastAccessToken(AccessToken lastAccessToken) {
        this.lastAccessToken = lastAccessToken;
    }
}

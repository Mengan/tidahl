package com.tmobile.tmoid.oauthplayground;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import com.tmobile.tmoid.Constants;
import com.tmobile.tmoid.helperlib.*;
import com.tmobile.tmoid.helperlib.util.Log;

import java.util.concurrent.TimeUnit;

/**
 * @author mihai.popoaei@movial.com
 */
public class OauthPlayground extends Activity {
    private static final String LOG_TAG = Constants.LOG_TAG;

    private AgentServiceConnection serviceConnection;
    private Handler handler;

    private View.OnClickListener createAgentConnectionAction = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        serviceConnection = new AgentServiceConnection(getApplicationContext(), 2, TimeUnit.SECONDS);

                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                update_buttons_states();
                            }
                        });
                    } catch (AgentNotFoundException e) {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                AgentServiceConnection.handleAgentNotFound(OauthPlayground.this);
                            }
                        });
                    } catch (AgentConnectionException e) {
                        e.printStackTrace();
                    }
                }
            }).start();
        }
    };

    ProgressDialog progressDialog;
    private View.OnClickListener createAgentConnectionActionAsync = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        start_loading();

                        try {
                            // just for show, to see the progress bar. Normally the connection is established very quick
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                        }

                        serviceConnection = new AgentServiceConnection(getApplicationContext(), new AgentServiceConnection.ConnectedCallback() {
                            @Override
                            public void onServiceConnected(AgentServiceConnection serviceConnection) {
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        update_buttons_states();
                                    }
                                });
                                stop_loading();
                            }
                        });
                    } catch (AgentNotFoundException e) {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                AgentServiceConnection.handleAgentNotFound(OauthPlayground.this);
                            }
                        });
                    }
                }
            }).start();
        }
    };

    private void stop_loading() {
        handler.post(new Runnable() {
            @Override
            public void run() {
                progressDialog.dismiss();
                progressDialog = null;
            }
        });
    }

    private void start_loading() {
        handler.post(new Runnable() {
            @Override
            public void run() {
                progressDialog = new ProgressDialog(OauthPlayground.this);
                progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
                progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progressDialog.setCancelable(false);
                progressDialog.setIndeterminate(true);
                progressDialog.setMessage("Connecting...");
                progressDialog.show();
            }
        });
    }

    private View.OnClickListener isUserAuthenticatedAction = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            handler.post(new Runnable() {
                @Override
                public void run() {
                    try {
                        Toast.makeText(OauthPlayground.this, "User authenticated: " +
                                serviceConnection.getAgent().requestAuthenticationStatus(),
                                Toast.LENGTH_SHORT).show();
                    } catch (IAMException e) {
                        Log.d(LOG_TAG, "while making Agent call: isUserAuthenticated", e);
                    }
                }
            });
        }
    };

    private View.OnClickListener getAuthenticatedUserIdentifierAction = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            handler.post(new Runnable() {
                @Override
                public void run() {
                    try {
                        Toast.makeText(OauthPlayground.this, "Authenticated user identifier: " +
                                serviceConnection.getAgent().requestUserIdentifier(),
                                Toast.LENGTH_SHORT).show();
                    } catch (IAMException e) {
                        Log.d(LOG_TAG, "while making Agent call: requestUserIdentifier", e);
                    }
                }
            });
        }
    };

    private AccessToken accessTokenResponse = null;
    private String authorization_code = null;
    private long accessTokenResponseRequestTime;
    private long authorizationCodeResponseRequestTime;

    private View.OnClickListener getAuthorizationCodeAction = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    if (invalidOauthParams()) {
                        startActivity(new Intent(OauthPlayground.this, SettingsActivity.class));

                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(OauthPlayground.this, "invalid OAuth param(s)", Toast.LENGTH_LONG).show();
                            }
                        });

                        return;
                    }

                    start_loading();

                    Spinner display = (Spinner) findViewById(R.id.f_auth_display);
                    Spinner accesstype = (Spinner) findViewById(R.id.f_auth_accesstype);
                    Spinner reauth = (Spinner) findViewById(R.id.f_auth_reauth);
                    Spinner approval_prompt = (Spinner) findViewById(R.id.f_auth_approval_prompt);

                    try {
                        authorization_code = serviceConnection.getAgent().requestAuthorizationCode(SettingsActivity.CLIENT_ID,
                                SettingsActivity.SCOPE,
                                accesstype.getSelectedItem().toString(),
                                display.getSelectedItem().toString(),
                                reauth.getSelectedItem().toString(),
                                approval_prompt.getSelectedItem().toString());

                        authorizationCodeResponseRequestTime = System.currentTimeMillis();

                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                if (authorization_code == null) {
                                    ((TextView) findViewById(R.id.f_authorizationCode)).setText("Authorization code: NULL");
                                } else {
                                    ((TextView) findViewById(R.id.f_authorizationCode)).setText(authorization_code);
                                }
                            }
                        });
                    } catch (RequestCanceledException rce) {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                ((TextView) findViewById(R.id.f_authorizationCode)).setText("API request has been canceled");
                                authorization_code = null;
                            }
                        });
                    } catch (final ServerErrorException ase) {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                ((TextView) findViewById(R.id.f_authorizationCode)).setText("API request failed due to server error: " + ase.toString());
                                authorization_code = null;
                            }
                        });
                    } catch (final IAMException e) {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                ((TextView) findViewById(R.id.f_authorizationCode)).setText(e.getMessage());
                                authorization_code = null;

                                Toast.makeText(OauthPlayground.this, "requestAuthorizationCode error:" + e.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        });
                        Log.d(LOG_TAG, "while making Agent call: requestAuthorizationCode", e);
                    } finally {
                        stop_loading();
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                update_buttons_states();
                            }
                        });
                    }
                }
            }).start();
        }
    };
    private View.OnClickListener getAccessTokenAction = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    if (invalidOauthParams()) {
                        startActivity(new Intent(OauthPlayground.this, SettingsActivity.class));

                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(OauthPlayground.this, "invalid OAuth param(s)", Toast.LENGTH_LONG).show();
                            }
                        });

                        return;
                    }

                    start_loading();

                    try {
                        accessTokenResponse = serviceConnection.getAgent().requestAccessToken(SettingsActivity.CLIENT_ID,
                                SettingsActivity.CLIENT_SECRET,
                                authorization_code);

                        accessTokenResponseRequestTime = System.currentTimeMillis();

                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                if (accessTokenResponse == null) {
                                    ((TextView) findViewById(R.id.f_accessToken)).setText("API response: NULL");
                                } else {
                                    ((TextView) findViewById(R.id.f_accessToken)).setText(accessTokenResponse.toString());
                                }
                            }
                        });
                    } catch (RequestCanceledException rce) {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                ((TextView) findViewById(R.id.f_accessToken)).setText("API request has been canceled");
                                accessTokenResponse = null;
                            }
                        });
                    } catch (final ServerErrorException ase) {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                ((TextView) findViewById(R.id.f_accessToken)).setText("API request failed due to server error: " + ase.toString());
                                accessTokenResponse = null;
                            }
                        });
                    } catch (final IAMException e) {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(OauthPlayground.this, "requestAccessToken error:" + e.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        });
                        Log.d(LOG_TAG, "while making Agent call: requestAccessToken", e);
                    } finally {
                        stop_loading();
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                update_buttons_states();
                            }
                        });
                    }
                }
            }).start();
        }
    };

    private boolean invalidOauthParams() {
        return SettingsActivity.CLIENT_ID == null || SettingsActivity.CLIENT_ID.equals("") ||
                SettingsActivity.CLIENT_SECRET == null || SettingsActivity.CLIENT_SECRET.equals("") ||
                SettingsActivity.SCOPE == null || SettingsActivity.SCOPE.equals("");
    }

    private View.OnClickListener queryProfileServer = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    start_loading();
                    try {
                        final ConsumerProfile profile_information = serviceConnection.getAgent()
                                .requestConsumerProfile(accessTokenResponse);

                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                String profile = wrapUpProfile(profile_information.toString());
                                ((TextView) findViewById(R.id.f_profileInformation)).setText(profile);
                            }
                        });
                    } catch (AccessTokenExpiredException atee) {
                        Log.d(LOG_TAG, "Access token expired");
                        Toast.makeText(OauthPlayground.this, "Access token expired", Toast.LENGTH_SHORT).show();
                    } catch (final IAMException iamEx) {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(OauthPlayground.this, "requestConsumerProfile error:" + iamEx.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        });
                        Log.d(LOG_TAG, "while making Agent call: requestConsumerProfile", iamEx);
                    } finally {
                        stop_loading();
                    }
                }
            }).start();
        }
    };

    private String wrapUpProfile(String profile) {
        while (profile.contains(":{")) {
            profile = profile.replaceFirst(":\\{", "{\n> ");
            profile = profile.replace(",", "\n> ");
        }

        return profile;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.oauthplayground);

        handler = new Handler();

        SharedPreferences preferences = getApplicationContext().getSharedPreferences(SettingsActivity.PREFERENCES_FILE, MODE_PRIVATE);
        if (preferences.getAll().size() == 3) {
            SettingsActivity.load(preferences);
        } else {
            try {
                SettingsActivity.load_defaults(getApplicationContext(), true);
            } catch (Exception e) {
                Log.d(LOG_TAG, "Error while loading default configuration", e);
            }
        }
        setup_spinners();

        findViewById(R.id.btn_createIAMAgentConnection).setOnClickListener(createAgentConnectionAction);
        findViewById(R.id.btn_createIAMAgentConnectionAsync).setOnClickListener(createAgentConnectionActionAsync);
        findViewById(R.id.btn_queryProfile).setOnClickListener(queryProfileServer);
        findViewById(R.id.btn_isUserAuthenticated).setOnClickListener(isUserAuthenticatedAction);
        findViewById(R.id.btn_getAuthenticatedUserIdentifier).setOnClickListener(getAuthenticatedUserIdentifierAction);
        findViewById(R.id.btn_getAccessToken).setOnClickListener(getAccessTokenAction);
        findViewById(R.id.btn_getAuthorizationCode).setOnClickListener(getAuthorizationCodeAction);
    }

    private void setup_spinners() {
        setup_spinner(R.id.f_auth_accesstype, R.array.access_token_accesstype);
        setup_spinner(R.id.f_auth_display, R.array.access_token_display);
        setup_spinner(R.id.f_auth_reauth, R.array.access_token_reauth);
        setup_spinner(R.id.f_auth_approval_prompt, R.array.access_token_reauth);
    }

    private void setup_spinner(int spinner_id, int resources_array) {
        Spinner spinner = (Spinner) findViewById(spinner_id);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                resources_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        update_buttons_states();
    }

    private void update_buttons_states() {
        boolean isConnected = serviceConnection != null;

        findViewById(R.id.btn_createIAMAgentConnection).setEnabled(!isConnected);
        findViewById(R.id.btn_createIAMAgentConnectionAsync).setEnabled(!isConnected);
        findViewById(R.id.btn_isUserAuthenticated).setEnabled(isConnected);

        boolean user_authenticated = false;
        try {
            if (serviceConnection != null) {
                user_authenticated = serviceConnection.getAgent().requestAuthenticationStatus();
            }
        } catch (IAMException e) {
            Log.d(LOG_TAG, "while making Agent call: requestAuthenticationStatus", e);
        }

        findViewById(R.id.btn_getAuthenticatedUserIdentifier).setEnabled(user_authenticated);

        findViewById(R.id.btn_getAccessToken).setEnabled(isConnected && (authorization_code != null));
        findViewById(R.id.btn_getAuthorizationCode).setEnabled(isConnected);
        findViewById(R.id.f_auth_display).setEnabled(isConnected);
        findViewById(R.id.f_auth_accesstype).setEnabled(isConnected);
        findViewById(R.id.f_auth_reauth).setEnabled(isConnected);
        findViewById(R.id.f_auth_approval_prompt).setEnabled(isConnected);

        boolean have_access_token = (accessTokenResponse != null && !accessTokenResponse.isExpired());

        findViewById(R.id.btn_queryProfile).setEnabled(isConnected && have_access_token);

        if (have_access_token) {
            ((TextView) findViewById(R.id.f_accessToken)).setText(accessTokenResponse.toString());
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater mi = getMenuInflater();
        mi.inflate(R.menu.oauthplayground, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_settings:
                startActivity(new Intent(this, SettingsActivity.class));
                break;
        }

        return super.onOptionsItemSelected(item);
    }
}

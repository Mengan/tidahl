package com.tmobile.tmoid.oauthplayground;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.XmlResourceParser;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import com.tmobile.tmoid.Constants;
import com.tmobile.tmoid.helperlib.util.Log;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;

/**
 * Author: mihai.popoaei@movial.com
 */
public class SettingsActivity extends Activity {
    private static final String LOG_TAG = Constants.LOG_TAG;

    public static String CLIENT_ID;
    public static String CLIENT_SECRET;
    public static String SCOPE;

    public static String PREFERENCES_FILE = SettingsActivity.class.getSimpleName() + "_preferences";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.oauthplayground_settings);
        setTitle(R.string.title_settings);

        ((TextView)findViewById(R.id.tf_client_id)).setText(CLIENT_ID);
        ((TextView)findViewById(R.id.tf_client_secret)).setText(CLIENT_SECRET);
        ((TextView)findViewById(R.id.tf_scope)).setText(SCOPE);

        findViewById(R.id.btn_action_save).setOnClickListener(action_save);
//        findViewById(R.id.btn_load_defaults_lab).setOnClickListener(action_load_defaults_lab);
        findViewById(R.id.btn_load_defaults_prod).setOnClickListener(action_load_defaults_prod);
    }

    private String savePrefString(SharedPreferences.Editor editor, String name, int field_id) {
        String field_value = ((TextView)findViewById(field_id)).getText().toString();
        Log.d(LOG_TAG, "saving to shared prefs:" + name + "=" + field_value);
        editor.putString(name, field_value);
        return field_value;
    }

    /**
     * save form to preferences
     */
    private void save_configuration() {
        Log.d(LOG_TAG, "save_configuration");
        SharedPreferences sp = getSharedPreferences(PREFERENCES_FILE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();

        CLIENT_ID = savePrefString(editor, "client.id", R.id.tf_client_id);
        CLIENT_SECRET = savePrefString(editor, "client.secret", R.id.tf_client_secret);
        SCOPE = savePrefString(editor, "scope", R.id.tf_scope);

        editor.commit();
        finish();
    }

    View.OnClickListener action_save = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            save_configuration();
        }
    };

    View.OnClickListener action_load_defaults_lab = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            load_defaults(SettingsActivity.this, false);
            populate_ui_fields();
        }
    };

    private void populate_ui_fields() {
        ((TextView)findViewById(R.id.tf_client_id)).setText(CLIENT_ID);
        ((TextView)findViewById(R.id.tf_client_secret)).setText(CLIENT_SECRET);
        ((TextView)findViewById(R.id.tf_scope)).setText(SCOPE);
    }

    View.OnClickListener action_load_defaults_prod = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            load_defaults(SettingsActivity.this, true);
            populate_ui_fields();
        }
    };

    public static void load(SharedPreferences sp) {
        CLIENT_ID = sp.getString("client.id", null);
        CLIENT_SECRET = sp.getString("client.secret", null);
        SCOPE = sp.getString("scope", null);

        Log.d(LOG_TAG, "loaded oauth settings from shared prefs:" + CLIENT_ID + "/" + CLIENT_SECRET + "/" + SCOPE);
    }

    /**
     * @param load_production_defaults
     */
    public static void load_defaults(Context iamAgentApplication, boolean load_production_defaults) {
        try {
            XmlResourceParser xml = iamAgentApplication.getResources().getXml(R.xml.oauthsettings);

            int eventType = xml.getEventType();
            while (eventType != XmlPullParser.END_DOCUMENT) {
                boolean tag_in_tag = false;

                if (eventType == XmlPullParser.START_TAG) {
                    String tagName = xml.getName();
                    eventType = xml.next();

                    if (eventType == XmlPullParser.START_TAG)
                        tag_in_tag = true;

                    if (eventType == XmlPullParser.TEXT) {
                        setValue(tagName, xml.getText());
                    }
                }

                if (!tag_in_tag)
                    eventType = xml.next();
            }

            Log.d(LOG_TAG, "loaded oauthplayground config xml file");
        } catch (IOException e) {
            Log.d(LOG_TAG, "while loading from xml", e);
        } catch (XmlPullParserException e) {
            Log.d(LOG_TAG, "while loading from xml", e);
        }
    }

    private static void setValue(String tagName, String value) {
        if ("client.id".equals(tagName)) {
            CLIENT_ID = value;
        } else if ("client.secret".equals(tagName)) {
            CLIENT_SECRET = value;
        } else if ("scope".equals(tagName)) {
            SCOPE = value;
        }
    }
}
